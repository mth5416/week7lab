import java.util.*;
import java.awt.event.*;
/**--------------------------------------------------------
 * KeyboardApp.java:
  * Pig Latin Translator
  * 
  * 
  * @author txr185
  */

public class KeyboardApp 
{
    //--------------------------------------------------------
    /** Creates a Frame and a KeyBoardApp. 
      *
      * @param args String[]
      */
    public static void main( String args[] )
    {
        String token = "";
        Scanner keyBoard = new Scanner(System.in);
        while(!token.equalsIgnoreCase("quit") && keyBoard.hasNext()){
            token = keyBoard.next();
            if(!token.equalsIgnoreCase("quit")) {
                System.out.println(StringUtilities.translate(token));
            }
        }
    }
}
